import turtle
import pandas as pd


def get_pos(string):
    """
    :param string: string containing all positions for the ploygon(s) that define a geographic entity
    :return: tuple of lists of (x, y) positions
    """
    res_list = []
    polygs = string.split('+')
    for pol in polygs:
        poslist = [item.strip(' M') for item in pol.split(' L ')]
        tups = [tuple(pos.split(',')) for pos in poslist]
        l = list(map(lambda a: [int(a[0]), -1*int(a[1])], tups))
    res_list.append(l)
    #for tup in tups:
        #x, y = int(tup[0]), -1*int(tup[1])
        #l.append((x, y))
    return res_list


df = pd.read_csv('EH_eskualdeak.csv', index_col=0)

coords = df.Koordenatuak


window = turtle.Screen()
window.screensize(20000, 20000)
window.bgcolor("white")
    
brad = turtle.Turtle()
brad.speed(0)
    
brad.pencolor("black")
brad.fillcolor('#008080')
brad.speed(1)


for string in coords.values:
    res_list = get_pos(string)
    for l in res_list:
        for i, (x, y) in enumerate(l):
            if i == 0:
                brad.begin_fill()
                brad.penup()
            else:
                brad.pendown()
            brad.goto(x, y)
        brad.end_fill()
        brad.pencolor("black")


window.exitonclick() 
