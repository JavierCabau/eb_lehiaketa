import pandas as pd
import requests
from bs4 import BeautifulSoup
from geojson import Polygon, dump
import json
import geopandas as gpd
import numpy as np
import os

# -Functions- #
def poly2geojson(poly, eremuaren_izena, multi=False):
    """
    Takes the coordinates of a (multi)polygon and writes geojson file ('eskualde_name.json')
    :param poly: coordinates
    :param eskualde: name of the eskualde
    :param multi: if True, the eskualde consists of more than one polygon
    :return: None
    """
    if not os.path.exists('geojson'):
        os.mkdir('geojson')

    if multi:
        dic = {"type": "MultiPolygon", 'coordinates': poly}
    else:
        dic = {"type": "Polygon", 'coordinates': poly}

    dic_json = json.dumps(dic)
    with open('geojson/{}.geojson'.format(eremuaren_izena), 'w') as f:
        f.write(dic_json)
    return

def write_geojson_files(df, eremu_mota):

    # Loop over column=izena in our dataframe, and write a geojson file for each of them
    for eremu in df.loc[:, eremu_mota].values:
        print(eremu, end=', ')
        # koord_string: eremuaren koordenatuak, string batean
        koord_string = df[df.loc[:, eremu_mota] == eremu].koordenatuak.values[0]
        # eremua osatzen duten poligonoen zerrenda
        eremu_poligonoak = koord_string.split('+')

        if len(eremu_poligonoak) > 1:  # multipolygon
            pol_list = []
            for pol in eremu_poligonoak:
                polygon = get_polygon_from_string(pol)
                pol_list.append(polygon)
            poly2geojson(pol_list, eremu, multi=True)

        else:  # single polygon
            polygon = get_polygon_from_string(eremu_poligonoak[0])
            poly2geojson(polygon, eremu)
    return


def get_polygon_from_string(string):
    polygon = [[[int(p.split(',')[0].strip().replace('M', '')),
             -1*int(p.split(',')[1].strip())] for p in string.split(' L ')]]
    return polygon


# -SCRAPING-
request = requests.get("http://www.soziolinguistika.eus/edb/index.php?erakus=mapa&zeremu=udalerri")
soup = BeautifulSoup(request.content, "html.parser")

items = soup.find_all("g")

list_names, list_coords = [], []
for it in items:
    # check if item is a group of items
    if it.find_all('title').__len__() == 1:
        list_names.append(it.title.string)
        paths = it.find_all('path')
        list_coords.append('+'.join([path['d'] for path in paths]))




# - FORMAT  - #
df = pd.read_csv('EH_udalerriak.csv')
# We replace '/' with '-' because later we create a separate geojson file for each eskualde,
# and when reading those files '/' is interpreted as a directory
df.loc[:, 'udalerri'] = [esk.replace('/', '-') for esk in df.loc[:, 'udalerri']]

# Loop over eskualdes in our dataframe, and write a geojson file for each of them
write_geojson_files(df, 'udalerri')

# Create single geopandas dataframe with all eskualdes
# by iteratively concatenating a dataframe with each eskualde

def merge_eremuak(df, eremu_mota):
    esk = df[eremu_mota].values[0]
    df_all = gpd.read_file('geojson/{}.geojson'.format(esk))
    df_all[eremu_mota] = esk
    for i, esk in enumerate(df[eremu_mota].values[1:]):
        df_esk = gpd.read_file('geojson/{}.geojson'.format(esk))
        df_esk[eremu_mota] = esk
        df_all = gpd.GeoDataFrame(pd.concat([df_all, df_esk]))
    df_all.set_index(np.arange(len(df_all)))
    df_all.eremu_mota = eremu_mota
    return df_all

df_udalerriak = merge_eremuak(df, 'udalerri')
df_udalerriak.to_file("geojson/EH_udalerriak.geojson", driver='GeoJSON')