import pandas as pd
import requests
from bs4 import BeautifulSoup
from geojson import Polygon, dump
import json
import geopandas as gpd
import numpy as np
import os

# ------------------------------- FUNTZIOAK -------------------------------------------
def poly2geojson(poly, eremu_mota, eremuaren_izena, multi=False):
    """
    Takes the coordinates of a (multi)polygon and writes geojson file ('eskualde_name.json')
    :param poly: coordinates
    :param eskualde: name of the eskualde
    :param multi: if True, the eskualde consists of more than one polygon
    :return: None
    """
    if not os.path.exists('geojson'):
        os.mkdir('geojson')

    if not os.path.exists('geojson/{}'.format(eremu_mota)):
        os.mkdir('geojson/{}'.format(eremu_mota))

    if multi:
        dic = {"type": "MultiPolygon", 'coordinates': poly}
    else:
        dic = {"type": "Polygon", 'coordinates': poly}

    dic_json = json.dumps(dic)
    with open('geojson/{}/{}.geojson'.format(eremu_mota, eremuaren_izena), 'w') as f:
        f.write(dic_json)

def write_geojson_files(df, eremu_mota):

    # Loop over column=izena in our dataframe, and write a geojson file for each of them
    for eremu in df.loc[:, eremu_mota].values:
        print(eremu, end=', ')
        # koord_string: eremuaren koordenatuak, string batean
        koord_string = df[df.loc[:, eremu_mota] == eremu].koordenatuak.values[0]
        # eremua osatzen duten poligonoen zerrenda
        eremu_poligonoak = koord_string.split('+')

        if len(eremu_poligonoak) > 1:  # multipolygon
            pol_list = []
            for pol in eremu_poligonoak:
                polygon = get_polygon_from_string(pol)
                pol_list.append(polygon)
            poly2geojson(pol_list, eremu_mota, eremu, multi=True)

        else:  # single polygon
            polygon = get_polygon_from_string(eremu_poligonoak[0])
            poly2geojson(polygon, eremu_mota, eremu)
    print('\n')
    return


def get_polygon_from_string(string):
    polygon = [[[int(p.split(',')[0].strip().replace('M', '')),
             -1*int(p.split(',')[1].strip())] for p in string.split(' L ')]]
    return polygon


def scrape_coords(eremu_mota):
    request = requests.get("http://www.soziolinguistika.eus/edb/index.php?erakus=mapa&zeremu={}".format(eremu_mota))
    soup = BeautifulSoup(request.content, "html.parser")

    items = soup.find_all("g")

    list_names, list_coords = [], []
    for it in items:
        # check if item is a group of items
        if it.find_all('title').__len__() == 1:
            list_names.append(it.title.string)
            paths = it.find_all('path')
            list_coords.append('+'.join([path['d'] for path in paths]))

    dic = {eremu_mota: list_names, 'koordenatuak': list_coords}
    df = pd.DataFrame(dic, columns=[eremu_mota, 'koordenatuak'])

    if eremu_mota == 'udalerri':
        # Udalerri batzuek izen bera dute bi herrialdetan: Bastidak eta Lekunberrik.
        df.iloc[487, :].udalerri = 'Lekunberri (Nafarroa Garaia)'
        df.iloc[706, :].udalerri = 'Lekunberri (Nafarroa Beherea)'

        df.iloc[20, :].udalerri = 'Bastida (Araba)'
        df.iloc[694, :].udalerri = 'Bastida (Nafarroa Beherea)'
        df.to_csv('EH_udalerriak.csv', index=False)

    df.loc[:, eremu_mota] = [erem.replace('/', '-') for erem in df.loc[:, eremu_mota]]
    df.to_csv('EH_{}ak.csv'.format(eremu_mota), index=False)

def merge_eremuak(df, eremu_mota):
    erem = df[eremu_mota].values[0]
    df_all = gpd.read_file('geojson/{}/{}.geojson'.format(eremu_mota, erem))
    df_all[eremu_mota] = erem
    for i, erem in enumerate(df[eremu_mota].values[1:]):
        df_erem = gpd.read_file('geojson/{}/{}.geojson'.format(eremu_mota, erem))
        df_erem[eremu_mota] = erem
        df_all = gpd.GeoDataFrame(pd.concat([df_all, df_erem]))
    df_all.set_index(np.arange(len(df_all)))
    df_all.eremu_mota = eremu_mota
    return df_all


# ------------------------------- HASIERA -------------------------------------------

for eremu_mota in ['udalerri', 'eskualde', 'herrialde']:
    print('* * * * * * * * * * * * EREMU MOTA: {}AK * * * * * * * * * * * * *'.format(eremu_mota.upper()))
    scrape_coords(eremu_mota)
    print('1. Koordenatuak jaitsi{}.....................................EGINA.'.format('.'*len(eremu_mota)))

    df = pd.read_csv('EH_{}ak.csv'.format(eremu_mota))
    print('2. {}-koordenatu sorta taula .csv formatuan gorde............EGINA.'.format(eremu_mota.capitalize()))
    write_geojson_files(df, eremu_mota)
    print('3. {} bakoitza .geojson formatuan gorde......................EGINA.'.format(eremu_mota.capitalize()))

    df_eskualdeak = merge_eremuak(df, eremu_mota)
    print('4. {}ak batu eta geopandas bakarrean gorde.....................EGINA.'.format(eremu_mota.capitalize()))

    df_eskualdeak.to_file("geojson/EH_{}ak.geojson".format(eremu_mota), driver='GeoJSON')
    print('5. {} guztiak geojson fitxategi bakarrean gorde..............EGINA.\n'.format(eremu_mota.capitalize()))

# - FORMAT  - #
# We replace '/' with '-' because later we create a separate geojson file for each eskualde,
# and when reading those files '/' is interpreted as a directory
# df.loc[:, eremu_mota] = [esk.replace('/', '-') for esk in df.loc[:, eremu_mota]]


# Loop over eskualdes in our dataframe, and write a geojson file for each of them

# Create single geopandas dataframe with all eskualdes
# by iteratively concatenating a dataframe with each eskualde





