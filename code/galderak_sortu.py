import random
import pandas as pd
import os
import geopandas as gpd
import matplotlib.pyplot as plt


def getGaldera(df):
    eremu_mota = df.columns.drop('geometry').values[0]
    iturria = 'Euskararen Datu Basea'
    galdera = 'Zein {} da?'.format(eremu_mota)
    link = 'http://www.soziolinguistika.eus/edb/index.php?erakus=mapa&zeremu={}'.format(eremu_mota)

    zuzena = getZuzena(df, eremu_mota)
    oker1, oker2 = getOkerrak(df, eremu_mota, zuzena)
    irudia = getIrudia(df, eremu_mota, zuzena, oker1, oker2)

    galdera_osoa = ';'.join([eremu_mota, galdera, irudia, zuzena, oker1, oker2, iturria, link])
    return galdera_osoa


def getZuzena(df, eremu_mota):
    selected = random.randint(0, df.shape[0]-1)
    return df.iloc[selected][eremu_mota]


def getOkerrak(df, eremu_mota, zuzena):
    oker1 = random.choice(df[eremu_mota].values)
    oker2 = random.choice(df[eremu_mota].values)
    while oker1 == zuzena:
        oker1 = random.choice(df[eremu_mota].values)
    while oker2 == zuzena or oker1 == oker2:
        oker2 = random.choice(df[eremu_mota].values)
    return oker1, oker2


def getIrudia(df, eremu_mota, zuzena, oker1, oker2):


    colors = ['#b21f66' if esk == zuzena else '#d6f0cd' for esk in df[eremu_mota].values]
    if eremu_mota == 'udalerri':
        lw = 0.1
    else:
        lw = 0.5
    df.plot(color=colors, edgecolor='black', linewidth=lw)
    plt.axis('off')
    idx_zuzena = df[df[eremu_mota] == zuzena].index.values[0]
    idx_oker1 = df[df[eremu_mota] == oker1].index.values[0]
    idx_oker2 = df[df[eremu_mota] == oker2].index.values[0]

    if not os.path.exists('../irudiak'): os.mkdir('../irudiak')
    if not os.path.exists('../irudiak/{}'.format(eremu_mota)): os.mkdir('../irudiak/{}'.format(eremu_mota))

    irudia_fname= '../irudiak/{}/{}_{}_{}_{}.png'.format(eremu_mota, eremu_mota, idx_zuzena, idx_oker1, idx_oker2)
    plt.savefig(irudia_fname, bbox_inches='tight')
    plt.close()
    return irudia_fname


def generateGalderak(df, irteera, zenbat):
    with open('../' + irteera, 'w') as f:
        f.write(';'.join(['Mota', 'Galdera', 'Irudia', 'Zuzena', 'Oker1', 'Oker2', 'Iturria', 'Esteka', '\n']))
        for i in range(zenbat):
            galdera_osoa = getGaldera(df)
            f.write(';'.join([galdera_osoa, '\n']))


# --- START --- #

df = gpd.read_file('geojson/EH_herrialdeak.geojson')
generateGalderak(df, 'herrialdeak_10galdera.csv', 10)

df = gpd.read_file('geojson/EH_eskualdeak.geojson')
generateGalderak(df, 'eskualdeak_50galdera.csv', 50)

df = gpd.read_file('geojson/EH_udalerriak.geojson')
generateGalderak(df, 'udalerriak_100galdera.csv', 100)






