import pandas as pd
import requests
from bs4 import BeautifulSoup
from geojson import Polygon, dump
import json
import geopandas as gpd
import numpy as np

# -Functions- #
def poly2geojson(poly, eskualde, multi=False):
    """
    Takes the coordinates of a (multi)polygon and writes geojson file ('eskualde_name.json')
    :param poly: coordinates
    :param eskualde: name of the eskualde
    :param multi: if True, the eskualde consists of more than one polygon
    :return: None
    """
    if multi:
        dic = {"type": "MultiPolygon", 'coordinates': poly}
    else:
        dic = {"type": "Polygon", 'coordinates': poly}

    dic_json = json.dumps(dic)
    with open('{}.geojson'.format(eskualde), 'w') as f:
        f.write(dic_json)
    return


def writeGeojson(df, izena):
    """
    :param df_guneak: pandas dataframe indexes: izena, Koordenatuak
    :param izena: str name of column (ej: 'eskualde')
    :return:
    """
    # Loop over column=izena in our dataframe, and write a geojson file for each of them
    for eremu in df.loc[:, izena].values:
        print(eremu)
        koord = df[df.loc[:, izena] == eremu].Koordenatuak
        eremu_pols = koord.values[0].split('+')

        if len(eremu_pols) > 1:  # multipolygon
            pol_list = []
            for pol in eremu_pols:
                polygon = [[[int(p.split(',')[0].strip().replace('M', '')), -1 * int(p.split(',')[1].strip())] for p in
                            pol.split(' L ')]]
                pol_list.append(polygon)
            poly2geojson(pol_list, eremu, multi=True)

        else:  # single polygon
            polygon = [[[int(p.split(',')[0].strip().replace('M', '')), -1 * int(p.split(',')[1].strip())] for p in
                        eremu_pols[0].split(' L ')]]
            poly2geojson(polygon, eremu)
    return


def clear_names(name):
    """
    This function clear the names
    """
    if '*' in name:
        # print(name)
        name = name.replace('*', '')
    if any(char.isdigit() for char in name):
        # print(name)
        name = ''.join(list(filter(lambda a: not a.isdigit(), name)))
    if len(name) <= 1:
        name = None
    return name

# -START-


# udalerriak ----------------------------------------------------------------------------------------------------------
request = requests.get("http://www.soziolinguistika.eus/edb/index.php?erakus=mapa&zeremu=udalerri")
soup = BeautifulSoup(request.content, "html.parser")

items = soup.find_all("g")

list_names = []
list_coords = []
for it in items:
    # list_names.append(it.find('title'))
    # check if item is a group of items
    if it.find_all('title').__len__() == 1:
        list_names.append(it.title.string)
        paths = it.find_all('path')
        list_coords.append('+'.join([path['d'] for path in paths]))

dic = {'Udalerria': list_names, 'Koordenatuak': list_coords}

df = pd.DataFrame(dic, columns=['Udalerria', 'Koordenatuak'])
df.to_csv('EH_udalerriak.csv', index=False)

########################## LIMPIAR PANDAST DATAFRAME ##################################



# ###################################### GEOPANDAS ################################ #

# eskualdeak ----------------------------------------------------------------------------------------------------------


# udalerriak ----------------------------------------------------------------------------------------------------------
# - FORMAT DATA - #
df = pd.read_csv('EH_udalerriak.csv')
# We replace '/' with '-' because later we create a separate geojson file for each eskualde,
# and when reading those files '/' is interpreted as a directory
df.loc[:, 'Udalerria'] = [esk.replace('/', '-') for esk in df.loc[:, 'Udalerria']]

# Loop over eskualdes in our dataframe, and write a geojson file for each of them
writeGeojson(df, 'Udalerria')

# Create single geopandas dataframe with all eskualdes
# by iteratively concatenating a dataframe with each eskualde
esk = df['Udalerria'].values[0]
df_places = gpd.read_file('{}.geojson'.format(esk))
df_places['Udalerria'] = esk
for i, esk in enumerate(df['Udalerria'].values[1:]):
    df_esk = gpd.read_file('{}.geojson'.format(esk))
    df_esk['Udalerria'] = esk
    df_places = gpd.GeoDataFrame(pd.concat([df_places, df_esk]))

df_places.set_index(np.arange(len(df_places)))
df_places.to_file("EH_udalerriak.geojson", driver='GeoJSON')



