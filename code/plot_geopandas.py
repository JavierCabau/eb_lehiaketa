
# -Libraries- #
import geopandas as gpd
from geojson import Polygon, dump
import json
import matplotlib.pyplot as plt
import os
import pandas as pd
import numpy as np
os.chdir('/home/amon/')


# -Functions- #
def poly2geojson(poly, eskualde, multi=False):
    """
    Takes the coordinates of a (multi)polygon and writes geojson file ('eskualde_name.json')
    :param poly: coordinates
    :param eskualde: name of the eskualde
    :param multi: if True, the eskualde consists of more than one polygon
    :return: None
    """
    if multi:
        dic = {"type": "MultiPolygon", 'coordinates': poly}
    else:
        dic = {"type": "Polygon", 'coordinates': poly}

    dic_json = json.dumps(dic)
    with open('{}.geojson'.format(eskualde), 'w') as f:
        f.write(dic_json)
    return



# esk_reshaped = df.eskualde.values.reshape(6, 7)  # 6 x 7 = 42 subplots (there are 42 eskualdes)
#     nrow, ncol = esk_reshaped.shape
#     fig, axs = plt.subplots(nrows=nrow, ncols=ncol, figsize=(2.5 * ncol, 2.5 * nrow))
#     for i in range(nrow):
#         for j in range(ncol):
#             selected_esku = esk_reshaped[i][j]
#             df.plot(ax=axs[i][j], color=[get_colDic(selected_esku)[esk] for esk in df.eskualde],
#                     edgecolor='white', linewidth=1)
#             # color: list comprehension to get a list of colors, ordered in the same order as the eskualdes in df
#             axs[i][j].set_title(selected_esku)
#             axs[i][j].axis('off')
#     plt.tight_layout()

# -START- #
#007BA7 #cerulean blue
# TODO:
# Arabako Ibarra sale dos veces. ¿Por qué?
# Geopandas: Ver si podemos meter el nombre de los poligonos
# Some names have spaces on both sides of '-' -> remove those
# No fuimos capaces de resolver el prolema de valid / invalid polygons
# todo: reading and formating geodataframe
"""
df = pd.read_csv('EH_eskualdeak.csv')

# Arabako Ibarra appears twice, so we remove the first occurrence
df = df.drop(axis=1, index=0)


# We replace '/' with '-' because later we create a separate geojson file for each eskualde,
# and when reading those files '/' is interpreted as a directory
df.loc[:, 'eskualde'] = [esk.replace('/', '-') for esk in df.loc[:, 'Eskualdea']]


# Loop over eskualdes in our dataframe, and write a geojson file for each of them
for eskualde in df.loc[:, 'eskualde'].values:
    print(eskualde)
    koord = df[df.loc[:, 'eskualde'] == eskualde].Koordenatuak

    eskualde_pols = koord.values[0].split('+')

    if len(eskualde_pols) > 1: # multipolygon
        pol_list = []
        for pol in eskualde_pols:
            polygon = [[[int(p.split(',')[0].strip().replace('M', '')), -1 * int(p.split(',')[1].strip())] for p in pol.split(' L ')]]
            pol_list.append(polygon)
        poly2geojson(pol_list, eskualde, multi=True)

    else:  # single polygon
        polygon = [[[int(p.split(',')[0].strip().replace('M', '')), -1 * int(p.split(',')[1].strip())] for p in
                    eskualde_pols[0].split(' L ')]]
        poly2geojson(polygon, eskualde)

# Create single geopandas dataframe with all eskualdes
# by iteratively concatenating a dataframe with each eskualde
esk = df['eskualde'].values[0]
df_places = gpd.read_file('{}.geojson'.format(esk))
df_places['eskualde'] = esk
for i, esk in enumerate(df['eskualde'].values[1:]):
    df_esk = gpd.read_file('{}.geojson'.format(esk))
    df_esk['eskualde'] = esk
    df_places = gpd.GeoDataFrame(pd.concat([df_places, df_esk]))

df_places.set_index(np.arange(len(df_places)))
"""

# todo: distances
"""
 esk = 'Pettarra'
#
# for i in range(len(df_places)):
#     if not df_places.iloc[i].geometry.is_valid:
#         df_places.iloc[i][0] = df_places.iloc[i].geometry.buffer(0)


a = df_places.iloc[0].geometry.representative_point()
for esk in df_places.eskualde.values:
    print(esk, end=' ')
    idx = np.argwhere(df_places.eskualde == esk)[0][0]
    dist = df_places.iloc[idx].geometry.representative_point().distance(a)
    print('Distance:', dist)


df_places[~df_places.geometry.disjoint(df_places.iloc[0].geometry)].eskualde

df_places[~df_places.geometry.disjoint(df_places.iloc[idx].geometry)].eskualde

df_places.iloc[0].geometry
"""



################################################################
# neighbor
# neighbors = df_[~df_.geometry.disjoint(country.geometry)].NAME.tolist()
#
# world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
# cities = gpd.read_file(gpd.datasets.get_path('naturalearth_cities'))
# country_shapes = world[['geometry', 'iso_a3']]
# country_names = world[['name', 'iso_a3']]
# countries = world[['geometry', 'name']]
# countries = countries.rename(columns={'name': 'country'})
#
# with open('miau.csv', 'w') as f:
#     f.write(df_places.to_json())
#
# df_places.to_file("EB_lehiaketa/EH_eskualdeak.geojson", driver='GeoJSON')

